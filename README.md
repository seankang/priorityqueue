# This is the use of Priority Queue without Lambda
This is the expanded version of this code from "Coding Interview practice"

        import java.util.*;

        public class FrequentKeyword {
            List<String> returnKFrequent(int k, String[] keywords, String[] reviews){
            List<String> result_set = new ArrayList<>();

            Map<String, Integer> map = new HashMap<>();
            for (String keyword: keywords){
                map.put(keyword, 0);
            }

            for (String review: reviews){
                for (String keyword: keywords){
                    if (review.toLowerCase().contains(keyword.toLowerCase())){
                        map.put(keyword, map.get(keyword) + 1);
                    }
                }
            }
            Queue<Map.Entry<String, Integer>> maxHeap = new PriorityQueue<>((a, b)-> a.getValue() == b.getValue() ? a.getKey().compareTo(b.getKey()) : b.getValue() - a.getValue());
            maxHeap.addAll(map.entrySet());
            return result_set;
        }

        public static void main(String[] args) {
            FrequentKeyword freq = new FrequentKeyword();
        
            String[] keywords = {"anacell", "cetracular", "betacellular"};
            String[] reviews = {"Anacell provides the best services in the city",
                    "betacellular has awesome services",
                    "Best services provided by anacell, everyone should use anacell"};
        
            System.out.println(freq.returnKFrequent(1, keywords, reviews));
        }
}