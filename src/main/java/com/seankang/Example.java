package com.seankang;
// Java program to demonstrate working of
// comparator based priority queue constructor
import java.util.*;

public class Example {
    public static void main(String[] args){

        PriorityQueue<Student> pq = new
                PriorityQueue<Student>(5, new StudentComparator());

        // Invoking a parameterized Student constructor with
        // name and cgpa as the elements of queue
        Student student1 = new Student("Nandini", 3.2);

        // Adding a student object containing fields
        // name and cgpa to priority queue
        pq.add(student1);
        Student student2 = new Student("Anmol", 3.6);
        pq.add(student2);
        Student student3 = new Student("Palak", 4.0);
        pq.add(student3);

        // Printing names of students in priority order,poll()
        // method is used to access the head element of queue
        System.out.println("Students served in their priority order");

        while (!pq.isEmpty()) {
            System.out.println(pq.poll().getName());
        }

        PriorityQueue<KeywordRanking> pq2 = new
                PriorityQueue<KeywordRanking>( new KeywordComparator());
        pq2.add (new KeywordRanking("anacell", 2));
        pq2.add (new KeywordRanking("cetracular", 0));
        pq2.add (new KeywordRanking("betacellular", 1));

        while (!pq2.isEmpty()) {
            System.out.println(pq2.poll().getName());
        }
    }
}

class StudentComparator implements Comparator<Student>{


    public int compare(Student s1, Student s2) {
        if (s1.cgpa < s2.cgpa)
            return 1;
        else if (s1.cgpa > s2.cgpa)
            return -1;
        return 0;
    }
}

class KeywordComparator implements Comparator<KeywordRanking>{


    public int compare(KeywordRanking o1, KeywordRanking o2) {
        if (o2.count == o1.count ){
            // if the count is the same, then we give the winner based on the keyword name
            return o2.keyword.compareTo(o1.keyword);
        }
        return o2.count - o1.count;

    }
}

class Student {
    public String name;
    public double cgpa;

    // A parameterized student constructor
    public Student(String name, double cgpa) {

        this.name = name;
        this.cgpa = cgpa;
    }

    public String getName() {
        return name;
    }
}

class KeywordRanking{
    public String keyword;
    public Integer count;

    public KeywordRanking(String  keyword, Integer rank){
        this.keyword = keyword;
        this.count = rank;
    }
    public String getName() {
        return keyword;
    }
}
